// Polinom.h
// ������ ������ ���������

#ifndef _POLINOM_H_
#define _POLINOM_H_

#include <iostream>
#include "HeadRing/HeadRing.h"
#include "Monom/Monom.h"

using namespace std;

class TPolinom : public THeadRing {
private:
	long GetPow(long val, int power);
public:
	TPolinom(int monoms[][2] = nullptr, int km = 0); // ����������� �������� 
													 // �� ������� ������������-������
	TPolinom(TPolinom &q); // ����������� �����������
	PTMonom  GetMonom()  { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // �������� ���������
	long CalculatePoly(int x, int y, int z); // ��������� �������� �������� 
	bool operator==(TPolinom &q); // ��������� ���������
	bool operator!=(TPolinom &q); // ��������� ���������
	TPolinom & operator=(TPolinom &q); // ������������
	friend ostream& operator<<(ostream &os, TPolinom &q); // ������ ��������
	friend istream& operator>>(istream &is, TPolinom &q); // ���� ��������
};
#endif
