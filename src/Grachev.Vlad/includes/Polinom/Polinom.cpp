// Polinom.cpp
// ������ ������ ���������

#include "Polinom/Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (int i = 0; i < km; i++)
	{
		pMonom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(pMonom);
	}
}

		/*-------------------------------------------*/

TPolinom & TPolinom::operator+(TPolinom &q) // �������� ���������
{
	PTMonom pm, qm, tm;
	Reset(); q.Reset();
	while (true)
	{
		pm = GetMonom(); qm = q.GetMonom();
		if (pm->Index < qm->Index)
		{
			// ������� ������ pm ������ �������� ������ qm 
			// >> ���������� ������ qm  � ������� p
			tm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(tm); q.GoNext();
		}
		else if (pm->Index > qm->Index)
			GoNext();
		else // ������� ������� �����
		{
			if (pm->Index == -1) // ������ �� ������ ���� �������������
				break; 
			pm->Coeff += qm->Coeff;
			if (pm->Coeff != 0)
			{
				GoNext(); q.GoNext();
			}
			else // �������� ������ � ������� �������������
			{
				DelCurrent(); q.GoNext();
			}
		}
	}
	return *this;
}

		/*-------------------------------------------*/

long TPolinom::GetPow(long val, int power)
{
	long result = 1;
	for (int i = 0; i < power; i++)
		result *= val;
	return result;
}

	/*-------------------------------------------*/

long TPolinom::CalculatePoly(int x, int y, int z) // ��������� �������� �������� 
{
	long result = 0;
	if (GetListLength() != 0)
	{
		PTMonom pMonom;
		int cf, index;
		for (Reset(); !IsListEnded(); GoNext())
		{
			pMonom = GetMonom();
			cf = pMonom->GetCoeff();
			index = pMonom->GetIndex();
			result += cf * GetPow(x, index / 100) * GetPow(y, (index % 100) / 10) * GetPow(z, index % 10);
		}
	}
	return result;
}

		/*-------------------------------------------*/

bool TPolinom::operator==(TPolinom &q) // ��������� ���������
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		TPolinom poly1, poly2;
		PTMonom pMon, qMon;
		Reset(); q.Reset();
		while (!IsListEnded())
		{
			pMon = GetMonom();
			qMon = q.GetMonom();
			if (*pMon == *qMon)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

		/*-------------------------------------------*/

bool TPolinom::operator!=(TPolinom &q) // ��������� ���������
{
	return !(*this == q);
}
		/*-------------------------------------------*/

TPolinom::TPolinom(TPolinom &q) // ����������� �����������
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for  ( q.Reset();  !q.IsListEnded(); q.GoNext())
	{
		pMonom = q.GetMonom();
		InsLast(pMonom->GetCopy());
	}
	q.Reset();
}

		/*-------------------------------------------*/

TPolinom & TPolinom::operator=(TPolinom &q) // ������������
{
	if (this != &q)
	{
		PTMonom pMonom;
		DelList();
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			pMonom = q.GetMonom();
			InsLast(pMonom->GetCopy());
		}
		q.Reset();
	}
	return *this;
}

		/*-------------------------------------------*/

ostream& operator<<(ostream &os, TPolinom &q)
{
	PTMonom pMonom;
	int cf, x, y, z;
	if (q.GetListLength() > 0)
	{
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			pMonom = q.GetMonom();
			cf = pMonom->GetCoeff();
			x = pMonom->GetIndex();
			y = (x % 100) / 10;
			z = x % 10;
			x = x / 100;
			if (cf != 0)
			{
				if ((cf > 0)&&(q.GetCurrentPos() != 0))
					os << "+" << cf;
				else
					os << cf;
				if (x > 0)
					os << "x^" << x;
				if (y > 0)
					os << "y^" << y;
				if (z > 0)
					os << "z^" << z;
				os << " ";
			}
		}
	}
	else
		os << 0;
	q.Reset();
	return os;
}

		/*-------------------------------------------*/

istream& operator>>(istream &is, TPolinom &q)
{
	if (q.GetListLength() > 0)
		q.DelList();
	char polystr[256];
	char mnstr[64];
	PTMonom tempMonom;
	int pos = 0, i, cf, x , y ,z;
	is.getline(polystr, 256);
	q.Reset();
	while (polystr[pos])
	{
		i = 0;
		while ((polystr[pos] != ' ') && (polystr[pos]))
		{	
			if (polystr[pos] != '+')
			{
				mnstr[i] = polystr[pos];
				i++;
			}
			pos++;
		}
		mnstr[i] = 0;
		i = 0;
		if (mnstr[0] == '-')
			i++;
		cf = x = y = z = 0; 
		while ((mnstr[i] != 'x') && (mnstr[i] != 'y') && (mnstr[i] != 'z') && (mnstr[i] != 0))
		{
			cf = cf * 10 + (mnstr[i] - '0');
			i++;
		}
		if (mnstr[0] == '-')
			cf *= -1;
		if (mnstr[i] == 'x')
		{
			i += 2;
			x = mnstr[i] - '0';
		}
		while ((mnstr[i] != 'y') && (mnstr[i] != 'z') && (mnstr[i] != 0))
			i++;
		if (mnstr[i] == 'y')
		{
			i += 2;
			y = mnstr[i] - '0';
		}
		while ((mnstr[i] != 'z') && (mnstr[i] != 0))
			i++;
		if (mnstr[i] == 'z')
		{
			i += 2;
			z = mnstr[i] - '0';
		}
		tempMonom = new TMonom(cf, x * 100 + y * 10 + z);
		q.InsLast(tempMonom);
		if (polystr[pos] != 0)
			pos++;
		mnstr[0] = 0;
	}
	q.Reset();
	return is;
}