#include <gtest/gtest.h>
#include "Polinom/Polinom.h"
#include "Polinom/Polinom.cpp"

TEST(TPolinom, can_create_polynomial)
{
	int monoms[][2] = { { 234, 234 }, { 230, 230 }, { 204, 204 }, { 24, 24 } };
	ASSERT_NO_THROW(TPolinom poly(monoms, 4));
}

TEST(TPolinom, equal_polynominals_are_equal)
{
	int monoms1[][2] = { { 234, 234 }, { 230, 230 }, { 204, 204 } };
	int monoms2[][2] = { { 234, 234 }, { 230, 230 }, { 204, 204 } };
	TPolinom poly1(monoms1, 3), poly2(monoms2, 3);
	ASSERT_TRUE(poly1 == poly2);
}

TEST(TPolinom, not_equal_polynominals_are_mot_equal)
{
	int monoms1[][2] = { { 234, 234 }, { 230, 230 }, { 204, 204 } };
	int monoms2[][2] = { { 24, 234 }, { 230, 230 } };
	TPolinom poly1(monoms1, 3), poly2(monoms2, 3);
	ASSERT_TRUE(poly1 != poly2);
}

TEST(TPolinom, can_create_polinominal_with_copy_constr)
{
	int monoms[][2] = { { 24, 234 }, { 230, 230 } };
	TPolinom poly1(monoms, 2);
	TPolinom poly2(poly1);
	ASSERT_TRUE(poly1 == poly2);
}

TEST(TPolinom, can_assign_polynominal)
{
	int monoms1[][2] = { { 234, 234 }, { 230, 230 }, { 204, 204 } };
	int monoms2[][2] = { { 24, 234 }, { 230, 230 } };
	TPolinom poly1(monoms1, 3), poly2(monoms2, 2);
	poly2 = poly1;
	ASSERT_TRUE(poly1 == poly2);
}

TEST(TPolinom, copied_polynominal_has_its_own_mem)
{
	int monoms1[][2] = { { 234, 234 }, { 230, 230 }, { 204, 204 } };
	int monoms2[][2] = { { 24, 234 }, { 230, 230 } };
	TPolinom poly1(monoms1, 3), poly2(monoms2, 2), poly3(poly2);;
	poly1 = poly2;
	poly2.DelFirst();
	ASSERT_FALSE((poly1 == poly2) || (poly2 == poly3));
}

TEST(TPolinom, can_sum_polynomial)
{
	int monoms1[][2] = { { 234, 234 }, { 230, 230 }, { 24, 24 }, { 1, 12 } };
	int monoms2[][2] = { { -234, 234 }, { -24, 24 } };
	int monoms3[][2] = { { 230, 230 }, {1, 12} };
	TPolinom poly1(monoms1, 4), poly2(monoms2, 2), respoly(monoms3, 2);
	poly1 = poly1 + poly2;
	ASSERT_TRUE(poly1 == respoly);
}

TEST(TPolinom, can_calculate_polynominal)
{
	int monoms[][2] = { { 4, 123 }, { -2, 110 }, { 4, 90 } };
	TPolinom poly1(monoms, 3), poly2;
	EXPECT_EQ(2296, poly1.CalculatePoly(2, 2, 2));
}

